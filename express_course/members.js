const members = [
    {
        id: 1,
        name: 'john',
        email: 'john@gmail.com',
        status: 'active'
    },
    {
        id: 2,
        name: 'shannon',
        email: 'shannon@gmail.com',
        status: 'inactive'
    },
    {
        id: 3,
        name: 'bob',
        email: 'bob@gmail.com',
        status: 'active'
    }
]

module.exports = members;