const fs = require('fs');
const path = require('path');

// Create folder
// fs.mkdir(path.join(__dirname,  '/test'), {}, error => {
//     if(error) throw error;
//     console.log("Folder created");
// });

// Create and write to file
// fs.writeFile(path.join(__dirname, '/test', 'hello.txt'),
//     'Hello World!',
//     error => {
//         if(error) {
//             throw error
//         } else {
//             console.log('File written');

//             // File append
//             fs.appendFile(path.join(__dirname, '/test', 'hello.txt'), ' I love NodeJS', error => {
//                 if(error) throw error;
//                 console.log('File written');
//             });
//         };
//     }
// );

// Read file
fs.readFile(path.join(__dirname, '/test', 'hello.txt'), 'utf8', (err, data) => {
    if(err) {
        throw err;
    } else {
        console.log(data);
    }
})

// Rename file
fs.rename(
    path.join(__dirname, '/test', 'hello.txt'),
    path.join(__dirname, '/test', 'helloworld.txt'), (err) => {
    if(err) {
        throw err;
    } else {
        console.log('File renamed...');
    }
})